using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System;
using ICSharpCode.SharpZipLib.Zip;

public class UsbCheck : MonoBehaviour
{


    public static List<DirectoryInfo> FindUSBDriver()
    {
        List<DirectoryInfo> finds = new List<DirectoryInfo>();
        DriveInfo[] drives = DriveInfo.GetDrives();
        foreach (DriveInfo drive in drives)
        {
            // 检查是否是可移动驱动器（USB设备）
            if (drive.DriveType == DriveType.Removable)
            {
                Debug.Log("USB Device Path: " + drive.RootDirectory);
                finds.Add(drive.RootDirectory);
            }
        }
        return finds;
    }

    public static IOrderedEnumerable<T> OrderByAlphaNumeric<T>( IEnumerable<T> source, Func<T, string> selector)
    {
        int max = source
                      .SelectMany(i => Regex.Matches(selector(i), @"\d+").Cast<Match>().Select(m => (int?)m.Value.Length))
                      .Max() ?? 0;

        return source.OrderBy(i => Regex.Replace(selector(i), @"\d+", m => m.Value.PadLeft(max, '0')));
    }


    public static string FindNewVersion(DirectoryInfo dir, string findKey)
    {
        var finds = new List<FileInfo>();
        finds = dir.GetFiles().Where(_ => Path.GetFileName(_.Name).Contains(findKey))?.ToList();

        var orderFinds = OrderByAlphaNumeric(finds, s => s.Name).ToList();

        if (orderFinds.Count() > 0)
        {
            return orderFinds.Last().FullName;
        }

        return string.Empty;
    }


}
