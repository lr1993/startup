using System.Collections;
using System.Collections.Generic;
using Sailfish;
using UnityEngine;

public class StartUpMgr : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Logic());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator Logic()
    {
        yield return new WaitForSeconds(0.1f);

        UISampleMgr.Show<OptionPanel>();
        UISampleMgr.Show<StartupPanel>();

        StartupLog.Log("启动端初始化");


    }
}
