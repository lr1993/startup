using System.Collections;
using System.Collections.Generic;
using Sailfish;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class StartupPanel : UISamplePanel
{

    public UIModule module;

    Toggle _isUseOFF;
    Toggle isUseOFF => _isUseOFF ??= module.TryGet<Toggle>("use");
    Toggle _usbOFF;
    Toggle usbOFF => _usbOFF ??= module.TryGet<Toggle>("usb");
    Toggle _resOFF;
    Toggle resOFF=> _resOFF ??= module.TryGet<Toggle>("res");
    Toggle _isAutoOFF;
    Toggle isAutoOFF=> _isAutoOFF ??= module.TryGet<Toggle>("auto");
    Button _isUpdateBTN;
    Button isUpdateBTN => _isUpdateBTN ??= module.TryGet<Button>("update");

    TMP_InputField _startup;
    TMP_InputField startup => _startup ??= module.TryGet<TMP_InputField>("startup");

    TMP_InputField _delay;
    TMP_InputField delay => _delay ??= module.TryGet<TMP_InputField>("delay");
    public override void Register()
    {
        
    }

    public override void Setup(params object[] vs)
    {
        isUseOFF.isOn  = BasicSetting.instance.isUsbUpdate;
        usbOFF.isOn    = BasicSetting.instance.isCans[0];
        resOFF.isOn    = BasicSetting.instance.isCans[1];
        isAutoOFF.isOn = BasicSetting.instance.isStartup;

        delay.text     = BasicSetting.instance.delayStartup.ToString();
        startup.text   = BasicSetting.instance.startupName;
        isUpdateBTN.onClick.AddListener(null);

        isUseOFF.onValueChanged.AddListener((_)=>
        {
            BasicSetting.instance.isUsbUpdate = _;
        });

         usbOFF.onValueChanged.AddListener((_)    =>
         {
         BasicSetting.instance.isCans[0]          = _;
         });

         resOFF.onValueChanged.AddListener((_)    => 
         {
         BasicSetting.instance.isCans[1]          =  _;
         });

         isAutoOFF.onValueChanged.AddListener((_) => 
        {
          BasicSetting.instance.isStartup   =  _;
        });

        delay.onEndEdit.AddListener((_)=>
        {
            BasicSetting.instance.delayStartup = float.Parse(_);
        });

        startup.onEndEdit.AddListener((_) =>
       {
           BasicSetting.instance.startupName = _;
       });


    }

    public override void UnRegister()
    {
       
    }


    void Update()
    { 
       
    }
}
