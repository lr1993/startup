using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartupLog : MonoBehaviour
{
    public TMP_Text m_log;
    public Transform m_parent; 
    internal static StartupLog instance;

    void Awake()
    {
        instance = this;
    }
    public static void Log(string log)
    {
        if (instance.m_log.mesh.vertexCount > 30000)
        {
            var go = GameObject.Instantiate(instance.m_log.gameObject);
            go.transform.SetParent(instance.m_parent, false);
            instance.m_log = go.GetComponent<TMP_Text>();
            instance.m_log.text = string.Empty;
        }
        instance.m_log.text += $"<color=red>[{System.DateTime.Now.ToString("HH:mm:ss")}]</color> {log}\n";
    }
}
