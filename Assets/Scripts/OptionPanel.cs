using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sailfish;
using TMPro;

public class OptionPanel : UISamplePanel,IClick,ISelect
{

    public UIModule module;

    public void Click(string key)
    {
        module.TryGet<TMP_Text>("SelectValue").text = key;
        module.TryGet<ButtonGroup>("btnGroups").GroupSelect(key);
    }

    public override void Register()
    {
       
    }

    public override void Setup(params object[] vs)
    {
        module.TryGet<ButtonGroup>("btnGroups").SetInitID(0);
    }

    public override void UnRegister()
    {
        
    }


    public void Select(string key)
    {
        module.TryGet<ButtonGroup>("btnGroups").GroupHover(key);
    }


    public void Deselect(string key)
    {
        Debug.Log("key:"+key);
        module.TryGet<ButtonGroup>("btnGroups").GroupNormal(key);

    }
}
