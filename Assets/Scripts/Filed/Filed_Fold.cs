using System.Collections;
using System.Collections.Generic;
using UnityEditor.Overlays;
using UnityEngine;
using UnityEngine.UI;

public class Filed_Fold : MonoBehaviour
{
    public Toggle toggle;

    public GameObject show;

    // Start is called before the first frame update
    void Awake()
    {
        toggle.onValueChanged.AddListener(ChangeValue);
    }

    private void OnEnable() 
    {
        ChangeValue(toggle.isOn);
    }

    void ChangeValue(bool ison)
    {
        show.SetActive(ison);
        var gps =  transform.GetComponentsInParent<LayoutGroup>();
        foreach (var item in gps)
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(item.GetComponent<RectTransform>());
        }
    }
}
