using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class ButtonState : MonoBehaviour, IPointerEnterHandler,IPointerClickHandler,IPointerExitHandler
{
    public enum State
    {
        Option,
        Button
    }

    public State state = State.Option;

    IClick  click;
    ISelect select;

    Transform[] states;

    // Start is called before the first frame update
    void Start()
    {
        click   = this.GetComponentInParent<IClick>(true);
        select  = this.GetComponentInParent<ISelect>(true);
        states  = this.transform.Find("State").GetTransformChildTypes();
    }



    public void Normal()
    {
        foreach (var item in states)
        {
            item.gameObject.SetActive(item.name.Contains("Normal"));
        }
    }


    public void Hover()
    {
       foreach (var item in states)
       {
            item.gameObject.SetActive(item.name.Contains("Hover"));
       }
    }

    public void Select()
    {
        foreach (var item in states)
        {
            item.gameObject.SetActive(item.name.Contains("Select"));
        }
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        if (state == State.Option) select?.Select(this.gameObject.name);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (state == State.Option) select?.Deselect(this.gameObject.name);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        click.Click(this.gameObject.name);
    }

   
}
