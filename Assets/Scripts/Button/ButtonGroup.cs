using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IClick
{
    void Click(string key);
}

public interface ISelect
{
    void Select(string key);
    void Deselect(string key);
}


public class ButtonGroup : MonoBehaviour
{

    List<ButtonState> states = new List<ButtonState>();

    public int currentID
    {
        get;
        private set;
    }

    public string currentKey
    {
        get;
        private set;
    }



 
    public void SetInitID(int id)
    {
        currentID  = id;
        states     = GetComponentsInChildren<ButtonState>(true)?.ToList();

        if (states != null)
        {
            currentKey = states[id].name;
            states[id] . Select();
        }
    }



    // public void Forword()
    // {
    //     Switch(+1);
    // }

    // public void ComeBack()
    // {
    //     Switch(-1);
    // }



    public void GroupHover(string name)
    {
        if (currentKey == name) return;

        if (states.Count() > 0)
        {
            //查找名字为state的ButtonState
            var state = states.Find(x => x.name == name);
            if (state != null)
            {
                if (currentID != states.IndexOf(state))
                    state.Hover();
            }
        }
    }

    public void GroupNormal(string name)
    {
        if (currentKey == name) return;

        if (states.Count() > 0)
        {
            //查找名字为state的ButtonState
            var state = states.Find(x => x.name == name);
            if (state != null)
            {
                if (currentID != states.IndexOf(state))
                    state.Normal();
            }
        }
    }

    public void GroupSelect(string name)
    {
        if (states.Count() > 0)
        {
            //查找名字为state的ButtonState
            var state = states.Find(x => x.name == name);

            if (state != null)
            {
                currentID = states.IndexOf(state);
                currentKey = name;

                for (int i = 0; i < states.Count; i++)
                {
                    if (currentID != i)
                        states[i].Normal();
                    else
                        states[i].Select();
                }
            }
        }
    }

    // void Switch(int value)
    // {
    //     currentID += value;
    //     if (currentID < 0)
    //         currentID += states.Count;
    //     if (currentID >= states.Count)
    //         currentID -= states.Count;


    //     for (int i = 0; i < states.Count; i++)
    //     {
    //         if (currentID != i)
    //         {
    //             states[i].DeHover();
    //         }
    //         else
    //         {
    //             states[i].Hover();
    //             currentKey = states[i].name;
    //         }
    //     }
    // }
}
