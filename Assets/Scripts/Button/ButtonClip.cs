using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonClip : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler,IPointerExitHandler
{

    Animator m_animator;
    public void OnPointerClick(PointerEventData eventData)
    {
      
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        m_animator = GetComponent<Animator>();
        m_animator.speed = 2;
        m_animator.transform.localScale = Vector3.one * 1.2f;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        m_animator       = GetComponent<Animator>();
        m_animator.speed = 1;
        m_animator.transform.localScale = Vector3.one;
    }
}
