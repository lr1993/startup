using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggle : MonoBehaviour
{
    Toggle toggle;

    Transform[] states;

    // Start is called before the first frame update
    void Awake()
    {
        states = this.transform.Find("State").GetTransformChildTypes();
        toggle = this.transform.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener(ChangeValue); 

    }

    private void OnEnable()
    {
        ChangeValue(toggle.isOn);
    }


    void ChangeValue(bool ison)
    {
        var str = ison ? "on" : "off";
        foreach (var item in states)
        {
            item.gameObject.SetActive(item.name.Contains(str));
        } 
    }
}
