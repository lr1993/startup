using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicSetting : Data
{
    public static BasicSetting instance;
    public string startupName;
    public bool isStartup;
    public float delayStartup;

    public bool isUsbUpdate;

    public bool[] isCans;

    void Start()
    {
        instance = this;
    }
    
}
