﻿//=======================================================
// 作者：LR
// 描述：
// 创建时间：2020-09-08 10:29:00
//=======================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public  class ViewMgr
{
    static Dictionary<System.Type, View> valuePairs = new Dictionary<System.Type, View>();

    public static T Get<T>() where T : View
    {
        if (valuePairs.TryGetValue(typeof(T),out View view))
        {
            return (T)view;
        }
        return  GameObject.FindObjectOfType<T>(true);
    }

    public static void Add<T>(T view) where T : View
    {
        if (!valuePairs.ContainsKey(view.GetType()))
        {
            valuePairs.Add(view.GetType(), view);
        }
    }

    public static void Remove<T>(T view) where T : View
    {
        if (valuePairs.ContainsKey(view.GetType()))
        {
            valuePairs.Remove(view.GetType());
        }
    }

    public static void Clear()
    {
        valuePairs.Clear();
    }
}



/// <summary>
/// MVC 结构 中的 控件模块
/// </summary>
public abstract class View : MonoBehaviour
{
    public virtual void Awake()
    {
        Debug.Log("加入View：" + this.GetType());
        ViewMgr.Add(this);
    }

    public virtual void OnDestroy()
    {
        ViewMgr.Remove(this);
    }
}

