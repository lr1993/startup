using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SingletonViewContainer
{
    public static Dictionary<string, SingletonView> views = new Dictionary<string, SingletonView>();

    public static void Push(SingletonView view)
    {
        views[view.GetType().Name] = view;
    }

    public static void Pop(SingletonView view)
    {
        if (views.ContainsKey(view.GetType().Name))
            views.Remove(view.GetType().Name);
    }
}


public class Singleton <T,V>
 where T   : new() 
 where V: SingletonView
{

    protected V  view
    {
        get
        {
            return View;
        }
    }

    public static V View
    {
        get
        {
            if (SingletonCreator.view == null)
            {
                if (SingletonViewContainer.views.TryGetValue((typeof(V).Name), out var s_view))
                {
                    SingletonCreator.view = (V)s_view;
                }
            }
            return SingletonCreator.view;
        }
    }

    public static T instance
    {
        get 
        {
             return SingletonCreator.instance; 
        }
    }

    class SingletonCreator
    {
        internal static V view;
        internal static readonly T instance = new T();
    }
}
