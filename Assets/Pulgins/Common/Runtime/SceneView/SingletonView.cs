using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonView : MonoBehaviour
{
    void Awake()
    {
        SingletonViewContainer.Push(this);
    }

    void OnDestroy()
    {
        SingletonViewContainer.Pop(this);
    }
}
