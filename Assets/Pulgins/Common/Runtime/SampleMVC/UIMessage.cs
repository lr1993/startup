﻿//=======================================================
// 作者：LR
// 公司：广州旗博士科技有限公司
// 描述：工具人
// 创建时间：#CreateTime#
//=======================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sailfish
{
	/// <summary>
    /// 消息类
    /// </summary>
	public class UIMsg
	{
		public object[] args;
		public UIMsg(params object[] args)
		{
			this.args = args;
		}
	}


    /// <summary>
    /// 消息类型和对应的委托
    /// </summary>
	public class TypeMsg
	{
		public Dictionary<System.Type, System.Action<UIMsg>> m_Msgs;
	}


	public class UIMessage  
	{
        //根据字段保存所有的消息类型和对应的委托
		static Dictionary<string, TypeMsg> m_Types = new Dictionary<string, TypeMsg>();


        /// <summary>
        /// 查找是否存在对应的消息类型
        /// </summary>
        /// <param name="msgKey">对应字段</param>
        /// <returns></returns>
		internal static TypeMsg FindType(string msgKey)
		{
			if (m_Types.TryGetValue(msgKey, out TypeMsg typeMsg))
			{
				return typeMsg;
			}
			else
			{
				typeMsg = new TypeMsg();
				typeMsg.m_Msgs = new Dictionary<System.Type, System.Action<UIMsg>>();
				m_Types[msgKey] = typeMsg;
				return typeMsg;
			}
		}


        /// <summary>
        /// 触发消息
        /// </summary>
        /// <param name="msgKey"></param>
        /// <param name="msg"></param>
		internal static void TriggerMsg(string msgKey, UIMsg msg)
		{
			var msgType = FindType(msgKey);

			foreach (var item in msgType.m_Msgs)
			{
				item.Value?.Invoke(msg);
			}
		}


        /// <summary>
        /// 添加消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="msgKey"></param>
        /// <param name="msgCall"></param>
		internal static void AddTypeMsg<T>(string msgKey, System.Action<UIMsg> msgCall)
		{
			var typeMsg = FindType(msgKey);
			typeMsg.m_Msgs[typeof(T)] = msgCall;
		}


        /// <summary>
        ///  移除消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="msgKey"></param>
		internal static void RemoveTypeMsg<T>(string msgKey)
		{
			var typeMsg = FindType(msgKey);

			if (typeMsg.m_Msgs.ContainsKey(typeof(T)))
			{
				typeMsg.m_Msgs.Remove(typeof(T));
			}
		}


        /// <summary>
        /// 删除T类的所有消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
		internal static void RemoveAllTypeMsg<T>()
		{
            foreach (var item in m_Types.Values)
            {
				if (item.m_Msgs.ContainsKey(typeof(T)))
				{
					item.m_Msgs.Remove(typeof(T));
				}
			}
		}

        /// <summary>
        /// 删除字段的所有消息
        /// </summary>
        /// <typeparam name="T"></typeparam>
		internal static void RemoveAllTypeMsg<T>(string msgKey)
		{
			if (m_Types.TryGetValue(msgKey, out TypeMsg typeMsg))
			{
				typeMsg.m_Msgs.Clear();
			}
			m_Types.Remove(msgKey);
		}

	}

	public static class UIMessageExtension
    {
		public static void AddTypeMsg<T>(this T target, string msgKey, System.Action<UIMsg> msgCall)
		{
			UIMessage.AddTypeMsg<T>(msgKey, msgCall);
		}

		public static void RemoveAllTypeMsg<T>(this T target)
		{
			UIMessage.RemoveAllTypeMsg<T>();
		}

		public static void RemoveTypeMsg<T>(this T target, string msgKey)
		{
			UIMessage.RemoveTypeMsg<T>(msgKey);
		}

		public static void RemoveAllTypeMsg<T>(this T target, string msgKey)
		{
			UIMessage.RemoveAllTypeMsg<T>(msgKey);
		}	

		public static void TriggerUIMsg<T>(this T target, string msgKey, UIMsg msg)
		{
			UIMessage.TriggerMsg(msgKey, msg);
		}

		public static void TriggerUIMsg(this UIMsg target, string msgKey)
		{
			UIMessage.TriggerMsg(msgKey, target);
		}
	}
}
