﻿//=======================================================
// 作者：LR
// 公司：广州纷享科技发展有限公司
// 描述：
// 创建时间：#CreateTime#
//=======================================================
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Sailfish
{

    [AttributeUsage(AttributeTargets.Class)]
    public class Sample : Attribute
    {
        public Type   mScript;
        public string mPath;
    }



    public class BaseUIPanel : MonoBehaviour
    {


        #region UI Panel和Panel之间的事件传输
        public void AddUIMsg(string msgKey, System.Action<UIMsg> msgCall)
        {
            this.AddTypeMsg(msgKey, msgCall);
        }

        public void RemoveUIMsg(string msgKey)
        {
            this.RemoveTypeMsg(msgKey);
        }

        public void RemoveAllMsg()
        {
            this.RemoveAllTypeMsg();
        }


        public void SendUIMsg(string msgKey, UIMsg uIMsg)
        {
            this.TriggerUIMsg(msgKey, uIMsg);
        }
        #endregion


        #region UI 外部往Panel的事件传输
        public void InitMethod()
        {
            this.AddMethodFuncs();
        }

        public void RemoveMethod()
        {
            if (UICall.GetInfoCount(this.GetHashCode()) > 0)
            {
                this.RemoveMethodFuncs();
            }
        }
        #endregion
    }


    public abstract class UISamplePanel: BaseUIPanel
    {
        private UISampleView _uIView;
        protected T Switch<T>() where T : UISampleView
        {
            if (_uIView == null)
            {
                _uIView = new UISampleView(transform);
                _uIView . Logic();
            }
            return (T)_uIView;
        }

        private void OnDisable()
        {
            UnRegister();
        }

        private void OnEnable()
        {
            Register();
        }

        void Start()
        {
            UISampleMgr.AddPanel(this.GetType(), this);
               //--Panel自带配置--
            InitMethod();
            //--Panel自带配置--
            OnStart();
        }

        void OnDestroy()
        {
            //--Panel自带配置--
            RemoveMethod();
            RemoveAllMsg();
            //--Panel自带配置--
            OnDelete();
        }


        public virtual void OnStart() { }
        public virtual void OnDelete() { }

        public abstract void Setup(params object[] vs);
        public abstract void Register();
        public abstract void UnRegister();
    }
}
