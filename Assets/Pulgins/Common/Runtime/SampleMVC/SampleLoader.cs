using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Sailfish
{
    public class SampleLoader 
    {
        #region 特性数据

        internal static Dictionary<System.Type, Sample> samples = new Dictionary<System.Type, Sample>();

        public static void AddLoaderInfo(Sample info)
        {
            if (!samples.ContainsKey(info.mScript))
            {
                samples.Add(info.mScript, info);
            }
        }
        public static Sample GetLoaderInfo(System.Type type)
        {
            Sample info = null;
            if (!samples.TryGetValue(type, out info))
            {
                //可能没有提取特性
                ExtractAttribute(type.Assembly);
                if (!samples.TryGetValue(type, out info))
                {
                    return null;
                }
            }
            return info;
        }
        public static void RemoveAllLoaderInfo()
        {
            samples.Clear();
        }

        /// <summary>
        /// 提取特性
        /// </summary>
        private static void ExtractAttribute(System.Reflection.Assembly assembly)
        {
            float start_time = Time.realtimeSinceStartup;
            //外部程序集
            List<System.Type> types = AttributeUtils.FindType<UISamplePanel>(assembly, true);


            if (types != null)
            {
                foreach (System.Type type in types)
                {
                    Sample ui_attr = AttributeUtils . GetClassAttribute<Sample>(type);
                    if (   ui_attr == null) continue;
                           ui_attr. mScript = type;
                    AddLoaderInfo(ui_attr);
                }
            }
            Debuger.Log("UIManager:ExtractAttribute 提取特性用时:" + (Time.realtimeSinceStartup - start_time));
        }

        #endregion
    }
}
