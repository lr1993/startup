﻿//=======================================================
// 作者：LR
// 公司：广州纷享科技发展有限公司
// 描述：
// 创建时间：#CreateTime#
//=======================================================
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Sailfish
{
    public class UISampleMgr : MonoBehaviour
    {
        static UISampleMgr instance;
        public bool isDontDestroy = false;

        public static Canvas uiCanvas;

        public static Camera uiCamera;



        private void Awake()
        {
            if (instance != null)
            {
                Destroy(instance.gameObject);
            }

            instance = this;

            if (isDontDestroy)
            {
                DontDestroyOnLoad(this.gameObject);
            }

            uiCanvas = this.GetComponent<Canvas>() ?? this.GetComponentInChildren<Canvas>();
            uiCamera = uiCanvas?.worldCamera;
        }


       


        internal static Dictionary<System.Type, UISamplePanel> panels = new Dictionary<System.Type, UISamplePanel>();

        public static Func<string,GameObject> OverrideLoadPanelFunction;

        internal static void AddPanel(System.Type type, UISamplePanel sample)
        {
            panels[type] = sample;
        }
        
        public static void Show<T>(bool isOverride = true, params object[] vs) where T : UISamplePanel
        {
            Debug.Log(panels.ContainsKey(typeof(T)));
            if (!panels.TryGetValue(typeof(T), out UISamplePanel panel))
            {
                var sample = SampleLoader.GetLoaderInfo(typeof(T));
                GameObject samplePanel = null;

                if (OverrideLoadPanelFunction != null && isOverride)
                {
                    samplePanel = OverrideLoadPanelFunction.Invoke(sample.mPath);
                }
                else
                {
                    samplePanel = Instantiate(Resources.Load(sample.mPath)) as GameObject;
                }

                samplePanel.transform.SetParent(instance.transform);
                samplePanel.transform.localScale       = Vector3.one;
                samplePanel.transform.localEulerAngles = Vector3.zero;
                samplePanel.transform.localPosition    = Vector3.zero;


                panel = samplePanel.GetComponent<T>() ?? samplePanel.AddComponent<T>();
                panels[typeof(T)] = panel;
            }

            panel.Setup(vs);
            panel.gameObject.SetActive(true);
        }
        public static void ShowNotOverride<T>( params object[] vs) where T : UISamplePanel
        {
            if (!panels.TryGetValue(typeof(T), out UISamplePanel panel))
            {
                var sample = SampleLoader.GetLoaderInfo(typeof(T));

                var samplePanel = Instantiate(Resources.Load(sample.mPath)) as GameObject;
               
                samplePanel.transform.SetParent(instance.transform);
                samplePanel.transform.localScale       = Vector3.one;
                samplePanel.transform.localEulerAngles = Vector3.zero;
                samplePanel.transform.localPosition    = Vector3.zero;

                panel = samplePanel.GetComponent<T>() ?? samplePanel.AddComponent<T>();
                panels.Add(typeof(T), panel);
            }

            panel.Setup(vs);
            panel.gameObject.SetActive(true);
        }

        public static void Hide<T>()
        {
            panels[typeof(T)].gameObject.SetActive(false);
        }


        private void OnDestroy()
        {
            panels.Clear();

            instance = null;
        }
    }
}
