﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sailfish;

    public class UISampleView
    {
        public Transform view;
        public UISamplePanel window;

        public void Set(bool action)
        {
            view.gameObject.SetActive(action);
        }

        public UISampleView(Transform target)
        {
            window = target.GetComponent<UISamplePanel>();
            view   = target.Find("View");
        }


        public virtual void Logic()
        {
    
        }

        public void AdaptScreen()
        {
            view.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        }
    }

