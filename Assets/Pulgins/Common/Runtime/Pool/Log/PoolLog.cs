using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sailfish;

namespace Pool.Log
{
    public class PoolLog : MonoBehaviour
    {
        internal PoolType type;
        internal string msg;
        internal int errorID = 0;

        void Start()
        {
            type = this.GetComponent<PoolType>();
        }

        private void Update()
        {
#if UNITY_EDITOR
            CheckError();
#endif
        }

        public void CheckError()
        {
            if (type.idleGos.Count + type.useGos.Count > type.maxCount && errorID == 0)
            {
                errorID = 1;
                PoolShowLog.AddLog(type, this);
            }
            if (type.idleGos.Count + type.useGos.Count <= type.maxCount && errorID == 1)
            {
                errorID = 0;
                PoolShowLog.RemoveLog(type);
            }


            if (errorID == 1)
                msg = $"(待机数组：{type.idleGos.Count})——(使用数组：{type.useGos.Count})——(最高数量：{type.maxCount})";
        }
    }
}