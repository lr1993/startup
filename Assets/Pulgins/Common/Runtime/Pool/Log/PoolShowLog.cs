using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Sailfish;

namespace Pool.Log
{
    [RequireComponent(typeof(PoolMgr))]
    public class PoolShowLog : MonoBehaviour
    {
        public class Error
        {
            public int arrayOut;
            public int error;

            public void Set(int id)
            {
                if (id == 1)
                    arrayOut += 1;
                else if (id == 2)
                    error     += 1;
            }
        }

        public class Log
        {
            public int type;
            public string msg;

            public Log(int error, string msg)
            {
                type = error;
                this.msg = msg;
            }
        }

        static Dictionary<PoolType, PoolLog> logs = new Dictionary<PoolType, PoolLog>();
        static Dictionary<string, Error> errors = new Dictionary<string, Error>();

        public static StringBuilder builder = new StringBuilder();
        public static StringBuilder builderTips = new StringBuilder();

        public static void AddLog(PoolType sort, PoolLog log)
        {
            if (!logs.ContainsKey(sort)) logs.Add(sort, log);

            Debug.Log("对象池异常");

            if (!errors.TryGetValue(sort.UseKey, out Error error))
            {
                error = new Error();
                errors.Add(sort.UseKey, error);
            }

            error.Set(log.errorID);

            Input();
        }

        public static void RemoveLog(PoolType sort)
        {
            if (logs.ContainsKey(sort))
            {
                logs.Remove(sort);
            }

            Debug.Log("取消");

           Input();
        }


        static void Input()
        {
            builder.Clear();
            builderTips.Clear();

            //正常输出
            foreach (var item in logs)
            {
                if (item.Value.errorID == 1)
                {
                    var log = string.Format("{0}_{1}", item.Key.UseKey, item.Value.msg);
                    builder.AppendLine(log);
                }
            }

            //异常汇总
            foreach (var item in errors)
            {
                var log = string.Format("Sort:{0}__(数组越界次数：{1})--(数组异常报空：{2})", item.Key, item.Value.arrayOut, item.Value.error);
                builderTips.AppendLine(log);
            }
        }


        void Update()
        {
#if UNITY_EDITOR
            Input();
#endif
        }

        private void OnDestroy()
        {
            logs.Clear();
            errors.Clear();
            builder.Clear();
            builderTips.Clear();
        }
    }
}
