using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class LoadingMgr : MonoBehaviour
{
    static LoadingMgr _instance;
    public static LoadingMgr instance
    {
        get 
        {
            if (_instance == null)
            {
                var loadPreload = Resources  . Load<GameObject>("Loading/Loading");
                var load        = GameObject . Instantiate(loadPreload);
                _instance       = load       . GetComponent<LoadingMgr>();
                DontDestroyOnLoad(load);
            }
            return _instance;
        }
    }

    void Awake()
    {
        _instance = this;
    }

    Slider _slider;
    Slider slider => _slider = _slider ?? transform.Find("Slider").GetComponent<Slider>();

    Text _text;
    Text text => _text = _text ?? transform.Find("Text").GetComponent<Text>();


    

    public async void Radio(float progress)
    {
        slider.value = progress;

        if (progress >= 1.0f)
        {
            text.text = "资源加载完成";
            await Task.Delay(500);
            Destroy();
        }
    }

    public void Destroy()
    {
        GameObject.Destroy(this.gameObject);
    }
}
