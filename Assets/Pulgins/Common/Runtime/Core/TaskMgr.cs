﻿//=======================================================
// 作者：LR
// 公司：广州纷享科技发展有限公司
// 描述：
// 创建时间：#CreateTime#
//=======================================================
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Sailfish
{
	public class TaskMgr : MonoSingleton<TaskMgr>
	{
        List<ITask> stackTasks = new List<ITask>();
        List<ITask> sortTasks  = new List<ITask>();

        ITask sortRunTask;
        ITask stackRunTask;


        public void Update()
        {
            //----------队列任务--------------
            if (stackTasks.Count > 0)
            {
                if (stackRunTask != null)
                {
                    stackRunTask.Update();

                    if (!stackRunTask.IsGaming)
                    {
                        stackTasks   . RemoveAt(0);
                        stackRunTask . Finish();
                        stackRunTask = null;
                    }
                }
                else
                {
                    if (!stackTasks[0].IsGaming)
                    {
                        stackRunTask = stackTasks[0];
                        stackRunTask.Run();
                    }
                }
            }

 

          
            //----------队列任务--------------





            //----------按layer排序任务--------------

            if (sortTasks.Count > 0)
            {
                if (sortRunTask != null)
                {
                    sortRunTask.Update();
                    if (!sortRunTask.IsGaming)
                    {
                        sortTasks.Remove(sortRunTask);
                        sortRunTask = null;
                    }
                }
                else
                {
                    if (!sortTasks[0].IsGaming)
                    {
                        sortRunTask = sortTasks[0];
                        sortRunTask.Run();
                    }
                }
            }
            //----------按layer排序任务--------------
        }


        public void AddStackTask(ITask task)
        {
            stackTasks.Add(task);
        }


        public void AddSortTask(ITask task)
        {
            sortTasks.Add(task);

            if (sortTasks.Count > 1)
            {
                sortTasks.Sort((x, y) =>
                {
                    if (x.Layer > y.Layer) return -1;
                    return 0;
                });
            }
        }


        private void OnDestroy()
        {
            sortRunTask ?.Finish();
            stackRunTask?.Finish();    

            sortRunTask  = null;
            stackRunTask = null;

            sortTasks  . Clear();
            stackTasks . Clear();
        }
    }




    public interface ITask
    {
        int Layer { get; }
        bool IsGaming{ get; }
        void Update();
        void Run();
        void Finish();
        void Input(params object[] vs);
    }

    public class BaseTask: CustomYieldInstruction, INotifyCompletion 
    {
        protected int layer;
        public    int Layer => layer;
        protected bool isCompleted = false;
        protected bool isGaming =  false;
        public    bool IsGaming => isGaming;

        #region  IEnumerator
        public override bool keepWaiting => !isCompleted;
        #endregion


        #region  Awaiter
        protected System.Action awaitEvent;
        public bool IsCompleted => isCompleted;
        public void GetResult() { } 
        public void OnCompleted(Action continuation) => awaitEvent = continuation;
        public BaseTask GetAwaiter() => this;
        #endregion
    }


    public class ItemTask : BaseTask, ITask
    {
        public ItemTask(int layer, params object[] vs)
        {
            this.layer  = layer;
            isCompleted = false;
            TaskMgr.instance.AddSortTask(this);
        }

        public ItemTask(params object[] vs)
        {
            layer       = 0;
            isCompleted = false;
            TaskMgr.instance.AddStackTask(this);
        }


        public virtual void Update()
        {  

        }

        public virtual void Input(params object[] vs)
        {

        }

        public virtual void Run()
        {
            isGaming    = true;
            isCompleted = false;
        }

        public virtual void Finish()
        {
            isGaming  = false;
            isCompleted = true;
            awaitEvent?.Invoke();
        }
    }
}
